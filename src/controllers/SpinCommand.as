package controllers
{
    import models.SpinModel;

    import org.robotlegs.mvcs.Command;

    import services.SlotConfigService;
    import services.SpinService;
    import services.WinService;

    public class SpinCommand extends Command
    {
        [Inject]
        public var model:SpinModel;

        [Inject]
        public var config:SlotConfigService;

        [Inject]
        public var spinService:SpinService;

        [Inject]
        public var winService:WinService;

        override public function execute():void
        {
            spinService.init(model, config);
            spinService.execute();

            winService.init(model, config);
            winService.execute();

            dispatch(new SlotEvent(SlotEvent.SPIN_COMPLETED, model));
        }
    }
}
