package controllers
{
    import flash.events.Event;

    public class SlotEvent extends Event
    {
        public static const SPIN_CLICKED:String = "spin_clicked";
        public static const SPIN_COMPLETED:String = "spin_completed";

        private var _body:Object;

        public function SlotEvent(type:String, body:Object = null)
        {
            super(type);
            _body = body;
        }

        public function get body():Object
        {
            return _body;
        }

        override public function clone():Event
        {
            return new SlotEvent(type, body);
        }
    }
}
