package services
{
    import constants.SymbolType;
    import constants.WinType;

    import models.ReelModel;

    import models.SpinModel;

    import org.robotlegs.mvcs.Actor;

    public class WinService extends Actor implements ISlotLogic
    {
        private var _model:SpinModel;
        private var _config:SlotConfigService;

        public function WinService()
        {
        }

        public function init(model:SpinModel, config:SlotConfigService):void
        {
            _model = model;
            _config = config;
        }

        public function execute():void
        {
            checkWins();
        }

        private function checkWins():void
        {
            checkWinLines();
            checkCherry();
            checkBonus();
        }

        private function checkWinLines():void
        {
            var winLines:Vector.<Vector.<String>> = getWinLines();
            var win:WinConfig;

            for each (var winLine:Vector.<String> in winLines)
            {
                win = getWin(winLine);
                if (win)
                {
                    _model.matchingRules.push(win.type);
                    _model.totalMultiplier += win.multiplier;
                }
            }
        }

        private function getWinLines():Vector.<Vector.<String>>
        {
            var symbolIndex:int;
            var symbolType:String;
            var lineSymbols:Vector.<String>;
            var winLines:Vector.<Vector.<String>> = new <Vector.<String>>[];

            for each (var lineConfig:LineConfig in _config.winLines)
            {
                lineSymbols = new <String>[];

                for (var reelIndex:int = 0; reelIndex < lineConfig.symbolsIndexes.length; reelIndex++)
                {
                    symbolIndex = lineConfig.symbolsIndexes[reelIndex];
                    symbolType = _model.reels[reelIndex].symbols[symbolIndex];

                    lineSymbols.push(symbolType);
                }

                winLines.push(lineSymbols);
            }

            return winLines;
        }

        private function getWin(winLine:Vector.<String>):WinConfig
        {
            for each (var winConfig:WinConfig in _config.wins)
            {
                if (compareVectors(winLine, winConfig.symbols))
                    return winConfig;
            }
            return null;
        }

        private function compareVectors(listA:Vector.<String>, listB:Vector.<String>):Boolean
        {
            for each (var object:String in listA)
            {
                if (listB.indexOf(object) == -1)
                    return false;
            }
            return true;
        }

        private function checkCherry():void
        {
            var winCounter:int;
            for each (var reel:ReelModel in _model.reels)
            {
                if (reel.symbols.indexOf(SymbolType.CHERRY) != -1)
                    winCounter++;

            }

            if (winCounter > 0)
            {
                _model.matchingRules.push(WinType.CHERRY);
                _model.totalMultiplier += _config.cherryPays[String(winCounter)];
            }
        }

        private function checkBonus():void
        {
            var winCounter:int;
            for each (var reel:ReelModel in _model.reels)
            {
                if (reel.symbols.indexOf(SymbolType.BONUS) != -1)
                    winCounter++;

            }

            if (winCounter == _model.reels.length)
            {
                _model.matchingRules.push(WinType.BONUS);
                _model.totalMultiplier += _model.matchingRules.length > 1 ? _config.bonusPays.multiple : _config.bonusPays.single;
            }
        }
    }
}
