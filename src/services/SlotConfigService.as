package services
{
    import org.robotlegs.mvcs.Actor;

    public class SlotConfigService extends Actor
    {
        [Embed(source="../../config/slot_config.json",mimeType="application/octet-stream")]
        private var JSONConfig:Class;

        private var _reels:Vector.<ReelConfig>;
        private var _cherryPays:Object;
        private var _bonusPays:BonusConfig;
        private var _winLines:Vector.<LineConfig>;
        private var _wins:Vector.<WinConfig>;

        public function SlotConfigService()
        {
            var config:Object = JSON.parse(new JSONConfig());

            parse(config);
        }

        private function parse(config:Object):void
        {
            _reels = new <ReelConfig>[];
            var reel:ReelConfig;
            for each (var reelObj:Object in config.reels)
            {
                reel = new ReelConfig(Vector.<String>(reelObj.stops), Vector.<int>(reelObj.weights));
                _reels.push(reel);
            }

            _winLines = new <LineConfig>[];
            for each (var line:Array in config.winLines)
            {
                _winLines.push(new LineConfig(Vector.<int>(line)));
            }

            _wins = new <WinConfig>[];
            for each (var win:Object in config.wins)
            {
                _wins.push(new WinConfig(win.type, Vector.<String>(win.symbols), win.multiplier));
            }

            _bonusPays = new BonusConfig(config.bonusPays.single, config.bonusPays.multiple);
            _cherryPays = config.cherryPays;
        }

        public function get reels():Vector.<ReelConfig>
        {
            return _reels;
        }

        public function get cherryPays():Object
        {
            return _cherryPays;
        }

        public function get winLines():Vector.<LineConfig>
        {
            return _winLines;
        }

        public function get wins():Vector.<WinConfig>
        {
            return _wins;
        }

        public function get bonusPays():BonusConfig
        {
            return _bonusPays;
        }
    }
}
