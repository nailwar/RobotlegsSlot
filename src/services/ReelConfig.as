package services
{
    public class ReelConfig
    {
        private var _stops:Vector.<String>;
        private var _weights:Vector.<int>;

        public function ReelConfig(stops:Vector.<String>, weights:Vector.<int>)
        {
            _stops = stops;
            _weights = weights;
        }

        public function get stops():Vector.<String>
        {
            return _stops;
        }

        public function get weights():Vector.<int>
        {
            return _weights;
        }

        public function getRandomIndex():int
        {
            var maxRandom:int;

            for each (var weight:int in _weights)
            {
                maxRandom += weight;
            }

            var random:int = getRandom(1, maxRandom);
            var totalWeight:int;

            for (var index:int = 0; index < _weights.length; index++)
            {
                totalWeight += _weights[index];

                if (totalWeight >= random)
                    break;

            }

            return index;
        }

        public function getSymbol(index:int):String
        {
            if (index < 0)
                index = _stops.length - 1;

            if (index >= _stops.length)
                index = 0;

            return _stops[index];
        }

        private function getRandom(min:int, max:int):int
        {
            return Math.ceil(Math.random()*(max - min)) + min;
        }
    }
}
