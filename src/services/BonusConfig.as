package services
{
    public class BonusConfig
    {
        private var _single:int;
        private var _multiple:int;

        public function BonusConfig(single:int, multiple:int)
        {
            _single = single;
            _multiple = multiple;
        }

        public function get single():int
        {
            return _single;
        }

        public function get multiple():int
        {
            return _multiple;
        }
    }
}
