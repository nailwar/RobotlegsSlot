package services
{
    public class LineConfig
    {
        private var _symbolsIndexes:Vector.<int>;

        public function LineConfig(symbolsIndexes:Vector.<int>)
        {
            _symbolsIndexes = symbolsIndexes;
        }

        public function get symbolsIndexes():Vector.<int>
        {
            return _symbolsIndexes;
        }
    }
}
