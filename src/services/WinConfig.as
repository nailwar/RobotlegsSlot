package services
{
    public class WinConfig
    {
        private var _type:String;
        private var _symbols:Vector.<String>;
        private var _multiplier:int;

        public function WinConfig(type:String, symbols:Vector.<String>, multiplier:int)
        {
            _type = type;
            _symbols = symbols;
            _multiplier = multiplier;
        }

        public function get type():String
        {
            return _type;
        }

        public function get symbols():Vector.<String>
        {
            return _symbols;
        }

        public function get multiplier():int
        {
            return _multiplier;
        }
    }
}
