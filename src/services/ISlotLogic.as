package services
{
    import models.SpinModel;

    public interface ISlotLogic
    {
        function init(model:SpinModel, config:SlotConfigService):void;
        function execute():void;
    }
}
