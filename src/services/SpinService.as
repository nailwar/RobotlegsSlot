package services
{
    import models.ReelModel;
    import models.SpinModel;

    import org.robotlegs.mvcs.Actor;

    public class SpinService extends Actor implements ISlotLogic
    {
        private var _model:SpinModel;
        private var _config:SlotConfigService;

        public function SpinService()
        {
        }

        public function init(model:SpinModel, config:SlotConfigService):void
        {
            _model = model;
            _config = config;
        }

        public function execute():void
        {
            spin();
        }

        private function spin():void
        {
            var index:int;
            var reel:ReelModel;

            for each (var reelConfig:ReelConfig in _config.reels)
            {
                index = reelConfig.getRandomIndex();

                var symbols:Vector.<String> = new <String>[];
                for (var i:int = index - 1; i <= index + 1; i++)
                {
                    symbols.push(reelConfig.getSymbol(i));
                }

                reel = new ReelModel(symbols);

                _model.reels.push(reel);
            }
        }
    }
}
