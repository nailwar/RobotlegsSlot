package
{

    import flash.display.Sprite;

    [SWF(width="320", height="240", backgroundColor="#E6ECFF", frameRate="60")]

    public class Slot extends Sprite
    {
        private var _context:SlotContext;

        public function Slot()
        {
            _context = new SlotContext(this);
        }
    }
}
