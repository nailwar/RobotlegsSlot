package
{
    import controllers.SlotEvent;
    import controllers.SpinCommand;

    import flash.display.DisplayObjectContainer;

    import models.SpinModel;

    import org.robotlegs.mvcs.Context;

    import services.SlotConfigService;
    import services.SpinService;
    import services.WinService;

    import views.Label;
    import views.LabelMediator;
    import views.SpinButton;
    import views.SpinButtonMediator;

    public class SlotContext extends Context
    {
        public function SlotContext(contextView:DisplayObjectContainer = null)
        {
            super(contextView);
        }

        override public function startup():void
        {
            commandMap.mapEvent(SlotEvent.SPIN_CLICKED, SpinCommand, SlotEvent);

            injector.mapClass(SpinModel, SpinModel);
            injector.mapSingleton(SlotConfigService);
            injector.mapSingleton(SpinService);
            injector.mapSingleton(WinService);

            mediatorMap.mapView(Label, LabelMediator);
            mediatorMap.mapView(SpinButton, SpinButtonMediator);

            var label:Label = new Label();
            label.x = 30;
            label.y = 30;
            contextView.addChild(label);

            var button:SpinButton = new SpinButton();
            button.x = 110;
            button.y = 160;
            contextView.addChild(button);

            super.startup();
        }
    }
}
