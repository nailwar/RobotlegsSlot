package models
{
    import org.robotlegs.mvcs.Actor;

    public class SpinModel extends Actor
    {
        public var reels:Vector.<ReelModel> = new <ReelModel>[];
        public var matchingRules:Vector.<String> = new <String>[];
        public var totalMultiplier:int;

        public function SpinModel()
        {
        }
    }
}
