package models
{
    public class ReelModel
    {
        private var _symbols:Vector.<String>;

        public function ReelModel(symbols:Vector.<String>)
        {
            _symbols = symbols;
        }

        public function get symbols():Vector.<String>
        {
            return _symbols;
        }
    }
}
