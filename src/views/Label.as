package views
{
    import flash.display.Sprite;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;

    public class Label extends Sprite
    {
        private var _textField:TextField;

        public function Label()
        {
            var textFormat:TextFormat = new TextFormat("Arial", 16);
            textFormat.align = TextFormatAlign.JUSTIFY;

            _textField = new TextField();
            _textField.defaultTextFormat = textFormat;
            _textField.autoSize = TextFieldAutoSize.LEFT;
            _textField.selectable = false;

            addChild(_textField);
        }

        public function set text(text:String):void
        {
            _textField.text = text;
        }
    }
}
