package views
{
    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;

    public class SpinButton extends Sprite
    {
        public function SpinButton()
        {
            useHandCursor = true;
            buttonMode = true;

            var textField:TextField = new TextField();
            textField.defaultTextFormat = new TextFormat("Arial", 16);
            textField.mouseEnabled = false;
            textField.autoSize = TextFieldAutoSize.CENTER;
            textField.text = "SPIN";

            var rectangleShape:Shape = new Shape();
            rectangleShape.graphics.beginFill(0x6699FF);
            rectangleShape.graphics.drawRect(0, 0, 100, 25);
            rectangleShape.graphics.endFill();

            addChild(rectangleShape);
            addChild(textField);
        }
    }
}
