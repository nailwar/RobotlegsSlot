package views
{
    import controllers.SlotEvent;

    import flash.events.MouseEvent;

    import org.robotlegs.mvcs.Mediator;

    public class SpinButtonMediator extends Mediator
    {
        public function SpinButtonMediator()
        {
        }

        override public function onRegister():void
        {
            eventMap.mapListener(view, MouseEvent.CLICK, onClick);
        }

        private function get view():SpinButton
        {
            return SpinButton(viewComponent);
        }

        private function onClick(event:MouseEvent):void
        {
            dispatch(new SlotEvent(SlotEvent.SPIN_CLICKED));
        }
    }
}
