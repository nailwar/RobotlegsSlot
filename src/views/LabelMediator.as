package views
{
    import controllers.SlotEvent;

    import models.ReelModel;

    import models.SpinModel;

    import org.robotlegs.mvcs.Mediator;

    public class LabelMediator extends Mediator
    {
        public function LabelMediator()
        {
        }

        override public function onRegister():void
        {
            eventMap.mapListener(eventDispatcher, SlotEvent.SPIN_COMPLETED, onSpinCompleted);
        }

        private function onSpinCompleted(event:SlotEvent):void
        {
            var model:SpinModel = SpinModel(event.body);
            var text:String = "Reel#1 Reel#2 Reel#3\n";

            for (var symbolIndex:int = 0; symbolIndex < model.reels[0].symbols.length; symbolIndex++)
            {
                for (var reelIndex:int = 0; reelIndex < model.reels.length; reelIndex++)
                {
                    text += model.reels[reelIndex].symbols[symbolIndex] + " ";
                }
                text += "\n";
            }

            text += "Matching rules: ";
            for (var i:int = 0; i < model.matchingRules.length; i++)
            {
                var matchingRule:String = model.matchingRules[i];
                text += matchingRule;

                if (i < model.matchingRules.length - 1)
                    text += ", ";
            }

            text += "\nTotal multiplier: " + model.totalMultiplier;

            view.text = text;
        }

        private function get view():Label
        {
            return Label(viewComponent);
        }
    }
}
